
# The irruption of the AI

<!-- badges: start -->
<!-- badges: end -->

Slides for the talk in the sesion _The role of Biostatistics in Health Data Science_ at the [2022 conference of the Spanish Society of Statistics and Operations Research (SEIO)](https://seio2022.com/en/), in Granda, Spain.

Slides on line: 

[![](img/cover.png)](https://umr-astre.pages.mia.inra.fr/presentations/irruption-ai)

Or [download the PDF version](https://umr-astre.pages.mia.inra.fr/presentations/irruption-ai/fm_irruption-ai.pdf)

